from django.urls import path, include
from rest_framework.routers import DefaultRouter
from droids import views

router = DefaultRouter()
router.register(r'parts', views.DroidPartViewSet, basename='parts')

urlpatterns = [
    path('', include(router.urls)),
    path('users/', views.UserCreate.as_view(), name='user_create'),
]
