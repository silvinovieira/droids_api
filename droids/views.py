from django.contrib.auth.models import User
from rest_framework.decorators import action
from rest_framework.generics import CreateAPIView
from rest_framework.permissions import IsAdminUser, IsAuthenticated
from rest_framework.response import Response
from rest_framework.viewsets import ModelViewSet
from .models import DroidPart
from .serializers import DroidPartSerializer, UserSerializer


class DroidPartViewSet(ModelViewSet):
    permission_classes = [IsAuthenticated]
    serializer_class = DroidPartSerializer

    def get_queryset(self):
        if self.request.user.is_staff:
            return DroidPart.objects.all()
        else:
            return self.request.user.parts.all()

    def perform_create(self, serializer):
        serializer.save(publisher=self.request.user)

    @action(detail=True, methods=['put'])
    def close(self, request, pk=None):
        part = self.get_object()
        part.status = 'CLOSED'
        part.save()
        return Response(DroidPartSerializer(part).data)


class UserCreate(CreateAPIView):
    permission_classes = [IsAdminUser]
    serializer_class = UserSerializer
