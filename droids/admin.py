from django.contrib import admin
from django.utils.safestring import mark_safe

from .models import DroidPart


class DroidPartAdmin(admin.ModelAdmin):
    list_display = ('publisher', 'description', 'contact_information', 'shipping_address', 'view_status')

    def view_status(self, obj):
        icons = {
            'OPEN': 'baseline-highlight_off.svg',
            'CLOSED': 'baseline-check_circle_outline.svg',
        }
        url = '/static/droids/{}'.format(icons[obj.status])
        return mark_safe('<img src="{}" />'.format(url))
    view_status.short_description = 'Is Closed?'

    def has_module_permission(self, request):
        return True

    def has_view_permission(self, request, obj=None):
        return True


admin.site.register(DroidPart, DroidPartAdmin)
