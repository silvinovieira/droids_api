from django.db import models


class DroidPart(models.Model):
    STATUS_CHOICES = [
        ('OPEN', 'Open'),
        ('CLOSED', 'Closed'),
    ]
    publisher = models.ForeignKey('auth.User', related_name='parts', on_delete=models.CASCADE)
    description = models.CharField(max_length=255)
    shipping_address = models.CharField(max_length=127)
    contact_information = models.CharField(max_length=127)
    status = models.CharField(max_length=6, choices=STATUS_CHOICES)

    def __str__(self):
        return '{}: {} - {}'.format(self.publisher.username, self.description, self.status)
