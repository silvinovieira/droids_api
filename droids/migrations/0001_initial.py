# Generated by Django 2.2.4 on 2019-08-04 14:36

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='DroidPartOrder',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('description', models.CharField(max_length=255)),
                ('shipping_address', models.CharField(max_length=127)),
                ('contact_information', models.CharField(max_length=127)),
                ('status', models.CharField(choices=[('OPEN', 'Aberta'), ('CLOSED', 'Finalizada')], max_length=6)),
            ],
        ),
    ]
