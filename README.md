# Droids API

## Prerequisites

- [Docker](https://www.docker.com/)
- [Docker Compose](https://docs.docker.com/compose/)

## Setup

Deploy containers in detached mode:
```shell script
 docker-compose up -d
```

Migrate database:
```shell script
 docker-compose exec rest-api python manage.py migrate --noinput
```

Serve static files:
```shell script
docker-compose exec rest-api python manage.py collectstatic --no-input --clear
```

Create superuser with:

- username: root
- email: [any]
- password: root
```shell script
 docker-compose exec rest-api python manage.py createsuperuser
```

## Usage

- Use the superuser created to login to Django Admin (`/admin`)
- Create new users
- Edit users and change their permissions

### Endpoints

- Port: `8000`
- API root: `/droids/`
- User creation helper: `/droids/users`
- Django admin: `/admin/`

### Users

- The superuser has full access
- A *Droids Administrator* is a user with "Staff status" permission and is able to:
    - Access to the Django Admin
    - View all parts on the Django Admin
    - Create new parts using the API
    - List all parts using the API
    - Modify and delete all parts using the API
- A *Droids Publisher* is a user without "Staff status" permission and is able to:
    - Create new parts using the API
    - List parts where they are the publisher using the API
    - Modify and delete parts where they are the publisher using the API
  
## Tests

The `postman` directory has a collection for testing the endpoints.
If the second an third tests fails check if users already existed (maybe from a previous run).
It is necessary to have an admin user with:

- username: root
- password: root
